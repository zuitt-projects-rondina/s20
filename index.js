console.log("Hello");


//JSON OBJECTS	
	//stands for Javascript Object Notation
	//can also be used in other programming languages
	//do not confuse Javascript objects with JSON objects
	//used for serializing different datatypes into bytes
		//serializiton - process of converting data types into series of bytes fo easier transfer of information
		//bytes are information that computer processes to perform different task
	// Uses double quotes for property name
		//Syntax
			/*
					"property" : "valueA",
					"propertyB" : "valueB"
			*/


// SAMPLE JSON OBJECTS
		
		/*{
			"city": "Quezon City",
			"province" : "Metro Manila",
			"country" : "Philippines"
		}
*/
		//JSON ARRAYS
	/*	"cities" : [
			{
				"city" : "Quezon City",
				"province" : "Metro Manila",
				"country" : "Philippines"

		}
		{
				"city" : "Cebu City",
				"province" : "Cebu",
				"country" : "Philippines"


		}

	];
*/
		//JSON METHODS
			//JSON OBJECTS CONTAINS METHOD FOR PARSING AND CONVERTING DATA INTO STRINGIFIED JSON


			let batchesArr = [
					{batchName: "BATCH 169"},
					{batchName: "BATCH 170"}


			];
			console.log(JSON.stringify(batchesArr));

			let data = JSON.stringify({

						name : "Luke",
						age : 45,
						address: {
							city : "Manila",
							country : "Philippines"
						}
					

					});
			console.log(data);

//USING STRINGIFY METHOD WITH VARIABLES
			//Sample user details

			let firstName = prompt("First Name");
			let lastName = prompt("Last Name");
			let age = prompt("Age");
			let address = {
				city : prompt("City"),
				country : prompt("Country")
			};

			let data2 = JSON.stringify({
					firstName : firstName,
					lastName : lastName,
					age : age,
					address : address

			});

			console.log(data2);

			// CONVERTING STRINGIFIED JSON INTO JAVASCRIPT OBJECTS SUING PARSE

			let batchesJSON = `[
				{	
					"id" : 1,

					"batchName" : "Batch 169",
					 "company": {
					        "name": "Hoeger LLC",
					        "catchPhrase": "Centralized empowering task-force",
					        "bs": "target end-to-end models"
    			}

				},
				{
					"batchName" : "Batch 170",
					"address": {
				        "street": "Skiles Walks",
				        "suite": "Suite 351",
				        "city": "Roscoeview",
				        "zipcode": "33263",
				        "geo": {
				          "lat": "-31.8129",
				          "lng": "62.5342"
        					}
     			 	}
				}
			]`

			console.log(JSON.parse(batchesJSON));

				